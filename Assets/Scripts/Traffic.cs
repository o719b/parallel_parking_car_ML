﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class TrafficPath
{
    public List<Transform> points;

    public List<Vector3> evaluated_points = null;

    public TrafficPath() { }
    public TrafficPath(List<Transform> points)
    {
        this.points = points;
    }

    public void EvaluatePoints(float time_step = 0.06f)
    {
        evaluated_points = new List<Vector3>();
        for (int i = 1; i < points.Count; i ++)
        {
            int steps = (int)(points[i - 1].position - points[i].position).magnitude / 4;
            for (int t = 0; t < steps; t++)
            {
                float time = (float)t / steps;

                Vector3 evaluation = Vector3.Lerp(points[i - 1].position, points[i].position, time);
                evaluated_points.Add(evaluation);
            }
        }
        evaluated_points.Add(points.Last().position);
    }
}

public class Traffic : MonoBehaviour
{
    public GameObject car_prefab;
    public float car_instance_span = 3.0f;
    public Transform traffic_parent;
    [Space]
    public List<TrafficPath> paths = new List<TrafficPath>();

    float instance_timer;
    private void Update()
    {
        if (instance_timer <= 0)
        {
            GameObject go = Instantiate(car_prefab, traffic_parent);
            TrafficPath next_path = paths[GetRandomPathIndex()];
            TrafficCar new_car = go.GetComponent<TrafficCar>();

            new_car.SetPath(next_path);

            instance_timer = car_instance_span;
        }

        instance_timer -= Time.deltaTime;
    }

    private int last_selected_path = -1;
    private int GetRandomPathIndex()
    {
        UnityEngine.Random.InitState((int)Time.timeSinceLevelLoad);
        int output = last_selected_path;
        while (output == last_selected_path)
            output = UnityEngine.Random.Range(0, paths.Count);

        last_selected_path = output;
        return output;
    }

    #region Other
    [ContextMenu("Create path")]
    public void CreatePaths()
    {
        paths = new List<TrafficPath>();

        List<Transform> ts = GetComponentsInChildren<Transform>().ToList();
        for (int i = 0; i < ts.Count; i++)
        {
            Transform t = ts[i];
            if (t.parent == transform)
            {
                TrafficPath path = new TrafficPath();
                path.points = new List<Transform>();

                List<Transform> lt = t.GetComponentsInChildren<Transform>().ToList();
                for (int tt = 0; tt < lt.Count; tt++)
                {
                    if (lt[tt].parent == t)
                        path.points.Add(lt[tt]);
                }
                paths.Add(path);
            }
        }
    }
    #endregion

    #region Debug
    private void OnDrawGizmos()
    {
        Vector3 off = Vector3.up * 0.05f;
        for (int i = 0; i < paths.Count; i++)
        {
            UnityEngine.Random.InitState(i);
            Gizmos.color = UnityEngine.Random.ColorHSV();

            TrafficPath path = paths[i];
            path.EvaluatePoints();

            for (int p = 1; p < path.evaluated_points.Count; p++)
            {
                Gizmos.DrawLine(path.evaluated_points[p - 1] + off, path.evaluated_points[p] + off);
                Gizmos.DrawWireSphere(path.evaluated_points[p] + off, 0.075f);
            }
        }
    }
    #endregion
}