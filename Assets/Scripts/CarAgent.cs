﻿using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using System;

// Главный класс агента (Главного автомобиля)
public class CarAgent : Agent
{
    public new Transform transform; // манипулятор физики
    public ArcadeCar car_movement; // компонент физики машины
    [Space]
    public ParkingLot lot; // компонент парковочного места
    public int observe_rays; // количество рейкастов для обозревания
    public float origin_y_offset;
    public LayerMask car_ignore;
    [Space]
    public Vector3 start_position; // начальные позиция и поворот при спавне (каждую эпоху)
    public Vector3 start_rotation;

    #region Mono
    private void Start() // метод, вызывающийся при запуске тренировки/отладки
    {
        // угол вращения каждого рейкаста = 360 / кол-во рейкастов
        observe_angle_step = 360 / observe_rays;

        // функция получения угла относительно парковки
        ReceiveParkFeedback = (float angle) =>
        {
            park_angle = angle;
        };
    }

    public const float STAY_TIME = 2f; // необходимое время нахождения на парковке для завершения эпохи

    Action<float> ReceiveParkFeedback;
    float speed, // скорость
    lot_dst = 0, // дистанция до парковочного места
    park_angle = 0, // угол относительно парковочного места
    stay_timer; // таймер времени стоянки
    bool park_attempt_granted = false;
    public bool last_park_status { get; private set; }

    private void Update() // метод, вызывающийся каждый кадр
    {
        lot_dst = (lot.transform.position - transform.position).magnitude; // вычисление расстояния до парковки
        speed = car_movement.GetSpeed(); // вычисление настоящей скорости
        bool park_status = lot.CheckParkStatus(this, lot_dst, ReceiveParkFeedback); // вычисление настоящего статуса парковки и получение необходимых данных через делегату ReceiveParkFeedback

        if (park_status) // если машина стоит на парковке
        {
            if (park_status != last_park_status) // если машина встала на парковку
            {
                if (!park_attempt_granted)
                {
                    AddReward(0.8f); // награждение агента за прохождение зоны парковки
                    park_attempt_granted = true;
                }
                else AddReward(-0.035f); // штраф за "виляние" на парковке
            }

            stay_timer -= Time.deltaTime;

            if (stay_timer <= 0) // машина успешно встала на парковку и простояла STAY_TIME сек
            {
                float dst_bonus = (ParkingLot.MAX_PARK_DISTANCE - lot_dst) / ParkingLot.MAX_PARK_DISTANCE, // вычисление бонуса за угол парковки
                    angle_bonus = (ParkingLot.MAX_PARK_ANGLE - park_angle) / ParkingLot.MAX_PARK_ANGLE; // вычисление бонуса за дистанцию от парковки

                AddReward(dst_bonus * 2.25f + angle_bonus * 3.5f); // награждение агента бонусами
                EndEpisode(); // заканчиваем эпизод (эпоху)
            }

            if (speed < previous_speed) AddReward(0.03f); // награждение агента за уменьшение скорости на парковке
        }
        else stay_timer = STAY_TIME;

        last_park_status = park_status;
        previous_speed = speed;
    }

    public const string ENV_TAG = "Environment";
    private void OnCollisionEnter(Collision collision) // когда машина сталкивается с объектами
    {
        if (collision.transform.tag == ENV_TAG)
        {
            AddReward(-4); // штрафуем
            EndEpisode(); // и сразу заканчиваем эпизод (эпоху)
        }
    }
    private void OnTriggerEnter(Collider other) // когда машина заезжает за "запретную" зону
    {
        if (other.tag == ENV_TAG)
            AddReward(-0.8f); // штрафуем
    }
    private void OnTriggerStay(Collider other) // пока машина находится на "запретной" зоне
    {
        if (other.tag == ENV_TAG)
            AddReward(-0.02f); // штрафуем
    }
    #endregion

    #region MLAgents
    public override void OnEpisodeBegin() // когда начинается эпизод (эпоха)
    {
        lot.OnNewEpisode?.Invoke();
        SpawnBox.OnNewEpisode?.Invoke();

        Vector3 shift = Vector3.forward * (UnityEngine.Random.value * 2 - 1) * 1.5f,
            episode_spawn_position = start_position + shift;
        car_movement.Reset(episode_spawn_position, start_rotation); // обнуление скорости, позиции, поворота машины
        park_attempt_granted = false;
    }

    private float previous_lot_dst, previous_speed;
    public override void OnActionReceived(ActionBuffers actions)
    {
        car_movement.direction.x = actions.DiscreteActions[0] - 1; // получение действия от нейронки по движению по горизонтали
        car_movement.direction.y = actions.DiscreteActions[1] - 1; // получение действия от нейронки по движению по вертикали

        if (StepCount % 10 == 0 && lot_dst > ParkingLot.MAX_PARK_DISTANCE)
            // награждение агента за уменьшение дистанции к парковке и штраф за увеличение 
            AddReward(lot_dst <= previous_lot_dst ? 0.005f : -lot_dst / 100.0f);
    }

    const int OBSERVER_DST = 25;

    int observe_angle_step = 0;
    List<float> observe_rays_dists = new List<float>();
    List<Vector3> observe_rays_dirs = new List<Vector3>();

    public override void CollectObservations(VectorSensor sensor)
    {
        float car_angle = transform.rotation.eulerAngles.y;

        observe_rays_dists = new List<float>();
        observe_rays_dirs = new List<Vector3>();

        Vector3 origin = transform.position + Vector3.up * origin_y_offset;

        // входные нейроны сети:
        sensor.AddObservation(RemoveYAxes(lot.transform.position - origin)); // отправка вектора относительно парковки
        sensor.AddObservation(park_angle); // отправка угла относительно парковки
        sensor.AddObservation(speed); // отправка текущей скорости машины
        sensor.AddObservation(last_park_status ? 0 : 1); // отправка статуса парковки

        for (int i = 0; i < observe_rays; i++) // цикл лучей
        {
            float angle = i * observe_angle_step - car_angle;
            float sin = Mathf.Sin(angle * Mathf.Deg2Rad),
                cos = Mathf.Cos(angle * Mathf.Deg2Rad);
            Vector3 global_direction = new Vector3(cos, 0, sin);

            Physics.Raycast(origin, global_direction, out RaycastHit hit, 
                OBSERVER_DST, car_ignore, QueryTriggerInteraction.Ignore);

            // 
            if (hit.transform)
            {
                float dst = hit.distance;
                sensor.AddObservation(dst); // отправка значения луча до объекта (если попадает куда-нибудь)
                observe_rays_dists.Add(dst); 
            }
            else
            {
                sensor.AddObservation(OBSERVER_DST); // отправка максимального значения луча (если никуда не попал)
                observe_rays_dists.Add(OBSERVER_DST); 
            }

            observe_rays_dirs.Add(global_direction);
        }

        Vector2 RemoveYAxes(Vector3 a) => new Vector2(a.x, a.z);
    }

    public override void Heuristic(in ActionBuffers actionsOut) // "ручное" управления машиной при выборе определённой настройки в компоненте
    {
        ActionSegment<int> input = actionsOut.DiscreteActions;
        input[0] = (int)Input.GetAxisRaw("Horizontal") + 1;
        input[1] = (int)Input.GetAxisRaw("Vertical") + 1;
    }
    #endregion

    #region Other
    [ContextMenu("Set new start transform")]
    public void SetNewStartTransform() // инструмент за сохранения стартовой позиции и поворота машины
    {
        start_position = transform.localPosition;
        start_rotation = transform.localRotation.eulerAngles;
    }
    #endregion

    #region Debug
    private void OnDrawGizmos() // отрисовка лучей
    {
        for (int i = 0; i < Mathf.Min(observe_rays_dirs.Count, observe_rays_dists.Count); i++)
        {
            float dst = observe_rays_dists[i];
            Vector3 dir = observe_rays_dirs[i];

            Gizmos.color = Color.Lerp(Color.red, Color.green, dst / OBSERVER_DST);
            Gizmos.DrawRay(transform.position + Vector3.up * origin_y_offset, dir);
        }
    }
    #endregion
}