﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBox : MonoBehaviour
{
    public List<TrafficCar> cars_in_box = new List<TrafficCar>();

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out TrafficCar car))
            cars_in_box.Add(car);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out TrafficCar car))
            cars_in_box.Remove(car);
    }

    public static Action OnNewEpisode;

    private void Start()
    {
        OnNewEpisode += delegate
        {
            cars_in_box.ForEach(x => Destroy(x.gameObject));
            cars_in_box = new List<TrafficCar>();
        };
    }
}
