﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TrafficCar : MonoBehaviour
{
    public float speed_factor = 4.5f;
    public LayerMask traffic_cars_mask;
    [Space]
    public new Transform transform;
    //public new Rigidbody rigidbody;

    public TrafficPath current_path { get; private set; }

    public const float NEXT = 0.075f, RAY_DST = 4;

    private Quaternion target_rot;
    private new bool enabled = false;
    private List<Vector3> points;
    private int cur_index = 0;
    private void Update()
    {
        if (cur_index >= points.Count)
        {
            enabled = false;
            Destroy(gameObject);
        }

        if (!enabled) return;

        float delta_time = Time.deltaTime;
        Vector3 cur_pos = transform.position, 
            target = points[cur_index],
            relative = target - cur_pos;

        if (relative.magnitude <= NEXT)
        {
            cur_index++;
            if (cur_index + 1 < points.Count)
                target_rot = Quaternion.LookRotation(points[cur_index + 1] - points[cur_index], Vector3.up);
        }

        float speed = speed_factor;
        if (Physics.Raycast(transform.position + Vector3.up, transform.forward, out RaycastHit hit,
            RAY_DST, traffic_cars_mask, QueryTriggerInteraction.Ignore))
        {
            speed *= 1 - ((hit.distance - 1.5f) / RAY_DST);
        }

        transform.position = Vector3.MoveTowards(cur_pos, target, delta_time * speed);
        transform.rotation = Quaternion.Lerp(transform.rotation, target_rot, delta_time * 3.5f);
    }

    public void SetPath(TrafficPath new_path)
    {
        current_path = new_path;
        points = current_path.evaluated_points;

        ResetForPoint();

        enabled = true;
    }

    public void ResetForPoint(int point_index = 0)
    {
        transform.position = points[point_index];
        transform.rotation = Quaternion.LookRotation(points[point_index + 1] -
            points[point_index]);
        cur_index = point_index;
    }
}