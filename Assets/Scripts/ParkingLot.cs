﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ParkingSpot
{
    public Vector3 local_pos;
    [Space]
    public GameObject spot_filler;
    public Transform spot_filler_transform;
    public Vector3 spot_filler_local_position;// { get; set; }

    public ParkingSpot(Vector3 local_pos)
    {
        this.local_pos = local_pos;
    }

    public void ResetSpotFillerTransform()
    {
        spot_filler_transform.localPosition = spot_filler_local_position;
        spot_filler_transform.localRotation = Quaternion.Euler(Vector3.zero);
    }
}

public class ParkingLot : MonoBehaviour
{
    public new Transform transform;
    public new SpriteRenderer renderer;
    [Space]
    public List<ParkingSpot> spots = new List<ParkingSpot>();
    [Space, Range(0, 100)]
    public float fill_chances;

    public const float MAX_PARK_DISTANCE = 1.25f;
    public const float MAX_PARK_ANGLE = 27.5f;

    public bool CheckParkStatus(CarAgent car, float precalculated_dst, Action<float> park_feedback)
    {
        Transform car_transform = car.transform;
        float distance = precalculated_dst,
            angle1 = Vector3.Angle(car_transform.forward, -transform.up),
            angle2 = Vector3.Angle(car_transform.forward, transform.up),
            angle = Mathf.Min(angle1, angle2);

        park_feedback?.Invoke(angle);

        bool output = distance <= MAX_PARK_DISTANCE && angle <= MAX_PARK_ANGLE;
        OnOutput?.Invoke(output);

        return output;
    }

    public Action<bool> OnOutput;
    public Action OnNewEpisode;

    private void Awake()
    {
        RememberObstaclesPositions();

        OnOutput = delegate (bool status)
        {
            renderer.color = status ? Color.green : Color.red;
        };

        OnNewEpisode = delegate
        {
            UnityEngine.Random.InitState((int)Time.timeSinceLevelLoad);
            int rand_ind = UnityEngine.Random.Range(0, spots.Count);
            transform.localPosition = spots[rand_ind].local_pos;

            for (int i = 0; i < spots.Count; i++)
            {
                ParkingSpot spot = spots[i];

                bool active = UnityEngine.Random.value * 100 <= fill_chances && i != rand_ind;
                if (active)
                {
                    spot.spot_filler.SetActive(true);
                    spot.ResetSpotFillerTransform();
                }
                else spot.spot_filler.SetActive(false);
            }
        };
    }

    #region Other
    [ContextMenu("Add lot position")]
    public void AddLotPosition()
    {
        spots.Add(new ParkingSpot(transform.localPosition));
    }

    [ContextMenu("Remember obstacles positions")]
    public void RememberObstaclesPositions()
    {
        for (int i = 0; i < spots.Count; i++)
        {
            Vector3 local_pos = spots[i].spot_filler_transform.localPosition;
            spots[i].spot_filler_local_position = local_pos;
        }
    }
    #endregion
}