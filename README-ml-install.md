# Установка и запуск обучения нейронной сети

1. Открыть консоль (cmd) из корневой директории проекта.
2. Выполнить команды:

   ```cmd
   python -m venv venv
   venv\Scripts\activate
   python -m pip install --upgrade pip
   pip install torch==1.7.0 -f https://download.pytorch.org/whl/torch_stable.html
   pip install mlagents
   pip install mlagents --use-feature=2020-resolver
   mlagents-learn
   ```

3. Запустить сцену в Unity.
